/**
 * @file
 * Trigger page reloading from Drupal backend.
 */

(() => {
  Drupal.AjaxCommands.prototype.pageReload = () => {
    // Reload the page without possible form resubmit:
    if (window.history.replaceState) {
      window.history.replaceState(null, null, window.location.href);
    }
    window.location = window.location.href;
  };
})();
