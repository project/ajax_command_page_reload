# Ajax Command Page Reload

## Usage
See [Drupal Core AJAX API](https://www.drupal.org/docs/drupal-apis/ajax-api) for general documentation.

~~~
$response = new AjaxResponse();
$response->addCommand(new PageReloadCommand());
return $response;
~~~
