<?php

namespace Drupal\ajax_command_page_reload\Ajax;

use Drupal\Core\Ajax\CommandInterface;
use Drupal\Core\Ajax\CommandWithAttachedAssetsInterface;
use Drupal\Core\Asset\AttachedAssets;

/**
 * Trigger "window.location.reload()" within the frontend.
 */
class PageReloadCommand implements CommandInterface, CommandWithAttachedAssetsInterface {

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    return [
      'command' => 'pageReload',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getAttachedAssets() {
    $assets = new AttachedAssets();
    $assets->setLibraries(['ajax_command_page_reload/ajax_commands']);
    return $assets;
  }

}
